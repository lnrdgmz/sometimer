import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Maybe exposing (andThen)
import List exposing (map)
import Platform.Cmd
import String exposing (split)
import Task
import Time exposing (Time, second, millisecond)

import Model exposing (..)

main =
    App.program
        { init = (init, Cmd.none)
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


-- Update

type Msg = SetTimer (Maybe ActiveTimer)
         | ToggleRunning
         | NextStep
         | ResumeTimer Time
         | Tick Time
         | Restart

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
      SetTimer r ->
        let newModel = { model
                           | currentTimer = r
                           , elapsedTime = toFloat 0 }
        in (newModel, Cmd.none)

      NextStep ->
        let
          newModel = nextStepModel model
        in
          (newModel, Cmd.none)
              
      ToggleRunning ->
        let newCmd =
              if
                model.running
              then
                Cmd.none
              else
                  startOrResumeTimerCmd
            newModel =
              { model | running = not model.running }
        in
            (newModel, newCmd)
                    
      ResumeTimer time ->
        let newModel =
              { model
                | running = True
                , lastUpdate = time
              }
        in (newModel, Cmd.none)
            
      Tick time ->
        let
            currentStepDuration = 
              Maybe.withDefault Time.hour <|
                model.currentTimer
                  `andThen`
                  (\ r -> List.head r.steps)
                  `andThen` (\ t -> Just t.duration)
            newElapsedTime =
              model.elapsedTime + time - model.lastUpdate
        in
            if newElapsedTime > currentStepDuration
            then (model
                 , Task.perform
                   identity
                   identity
                   (Task.succeed NextStep))
            else
              let newModel =
                    { model
                      | elapsedTime = newElapsedTime
                      , lastUpdate = time
                    }
              in
                  (newModel, Cmd.none)
      Restart ->
          let
              resetTimers timer =
                  { timer
                      | steps = List.reverse timer.pastSteps
                                 ++ timer.steps
                      , pastSteps = []
                  }
              newModel =
                  { model
                      | currentTimer =
                          Maybe.map resetTimers model.currentTimer
                      , elapsedTime = toFloat 0
                      , running = False
                  }
                        
          in
              (newModel, startOrResumeTimerCmd)

startOrResumeTimerCmd =
    -- First argument is meaningless error-handler. The task `Time.now`
    -- shouldn't ever fail.
    Task.perform identity ResumeTimer Time.now

nextStepModel model = 
  case model.currentTimer of
      Nothing ->
        model

      Just timer ->
        let
          newTimer = 
            case timer.loops of
                True ->
                  Just { timer
                         | steps = List.drop 1 timer.steps ++
                                    List.take 1 timer.steps
                       }
                False ->
                  case List.tail timer.steps of
                      Nothing ->
                        Nothing
                      Just lst ->
                        Just { timer
                               | steps = lst
                                 , pastSteps =
                                   List.take 1 timer.steps ++
                                   timer.pastSteps
                             }
          newRunning =
            not <| newTimer == Nothing
            
        in
            { model
              | elapsedTime = toFloat 0
              , currentTimer = newTimer
            }

-- Subscriptions

subscriptions : Model -> Sub Msg
subscriptions model =
  if
    model.running
  then
    -- Time.every (10 * millisecond) Tick
      Time.every second Tick
  else
    Sub.none

-- View

view : Model -> Html Msg
view model =
  mainDiv <| List.append skeletonCssLinks <|
    case model.currentTimer of
      Nothing ->
        timerChooser

      Just timer ->
        if
          timer.steps == []
        then
          finishedView timer
        else if
          timer.pastSteps == []
              && model.elapsedTime == toFloat 0
        then
          previewView timer
        else
          let
            renderCurrentStep timer =
                case List.head timer.steps of
                    Nothing ->
                        div [] []
                    Just step ->
                        currentStepView step model.elapsedTime
            renderNextStep timer =
            case List.head (List.drop 1 timer.steps) of
                Nothing ->
                    div [] []
                Just step ->
                    nextStepView step
            in
                    [ h1 [] [ text timer.name ]
                    , renderCurrentStep timer
                    , renderNextStep timer
                    , button [ onClick ToggleRunning ]
                        [ text <| if model.running
                                  then "Pause"
                                  else "Resume"
                        ]
                    , button [ onClick NextStep ]
                        [ text "Skip >>" ]
                    ]
mainDiv =
  div [ class "container" ]
                    
currentStepView step elapsedTime =
  div [ ]
    [ h2 [] [ text step.title ]
    , text <| formattedTime (step.duration - elapsedTime)
    ]

nextStepView step =
  div []
    [ h3 [] [ text <| "Next up: " ++ step.title ] ]

formattedTime time =
  let
    timeInt = ceiling <| Time.inSeconds time
    secs = timeInt % 60
    mins = (timeInt // 60) % 60
    hrs = timeInt // 3600
  in
      if hrs > 0
      then toString hrs ++ ":"
      else ""
      ++ String.padLeft 2 '0' (toString mins)
      ++ ":"
      ++ String.padLeft 2 '0' (toString secs)
    
timerChooser =
    h1 [] [ text "Select a timer:"]
    :: map timerSelectionButton availableTimers

timerSelectionButton timer =
    button
        [ onClick (SetTimer (Just <| timerToActiveTimer timer))
        , class "button button-primary"
        ]
        [ text timer.name ]

finishedView timer =
    [ h1 [] [ text timer.name ]
    , h2 [] [ text "You're done!" ]
    , button [ onClick Restart ]
        [ text "Restart" ]
    , button [ onClick (SetTimer Nothing) ]
        [ text "View Timers" ]
    ]

previewView timer =
    [ h1 [] [ text timer.name ]
    , p [] [ text timer.description ]
    , button [ onClick ToggleRunning ]
        [ text "Start Timer" ]
    , button [ onClick (SetTimer Nothing) ]
        [ text "View Timers" ]
    ]


skeletonCssLinks =
  [ node "link"
      [ href "//fonts.googleapis.com/css?family=Raleway:400,300,600"
      , rel "stylesheet"
      , type' "text/css"
      ] [] 
  , node "link"
    [ rel "stylesheet"
    , href "css/normalize.css"
    ] []
  , node "link"
    [ rel "stylesheet"
    , href "css/skeleton.css"
    ] []
  ]
