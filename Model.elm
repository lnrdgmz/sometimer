module Model exposing (..)

import Time exposing (Time, second, minute)

-- Model
    
type alias Model =
    { currentTimer : Maybe ActiveTimer
    , running : Bool
    , elapsedTime : Time
    , lastUpdate : Time
    }

type alias ActiveTimer =
    { name : String
    , description : String
    , steps : List Step
    , loops : Bool
    , pastSteps : List Step
    }

timerToActiveTimer r =
    ActiveTimer
        r.name
        r.description
        r.steps
        r.loops
        []

init : Model
init = Model Nothing False 0 0

type alias Timer =
    { name : String
    , description : String
    , steps : List Step
    , loops : Bool
    }

type alias Step =
    { title : String
    , duration : Time
    }

availableTimers =
  [ pomodoro, fastPomodoro, sevenMinuteWorkout, quickFiniteTimer ]

quickFiniteTimer =
    Timer
        "Quick Finite Timer"
        "A quick timer for testing purposes"
        [ (Step "Step 1" (second * 3))
        , (Step "Step 2" (second * 3))
        , (Step "Step 3" (second * 3))
        ]
        False

fastPomodoro : Timer
fastPomodoro =
    let
        rest = Step "Rest" (second * 5)
        study = Step "Study" (second * 5)
    in
        Timer "Fast Pomodoro"
            "An accelerated pomodoro, for testing the timer."
            [study, rest, study, rest]
            True

pomodoro : Timer
pomodoro =
    let
        rest = Step "Rest" (minute * 5)
        study = Step "Study" (minute * 20)
    in
        Timer "Pomodoro"
            "For practising the pomodoro technique."
            [study, rest, study, rest]
            True

sevenMinuteWorkout : Timer
sevenMinuteWorkout =
  let exc = \ name -> Step name (30 * second)
      rest = Step "Rest" (5 * second)
  in
      Timer
         "Seven Minute Workout"
         "A full-body workout, in just seven minutes."
         [ exc "Jumping Jacks"
         , rest
         , exc "Wall Sit"
         , rest
         , exc "Push Ups"
         , rest
         , exc "Abdominal Crunches"
         , rest
         , exc "Step-ups Onto Chair"
         , rest
         , exc "Triceps Dip on Chair"
         , rest
         , exc "Plank"
         , rest
         , exc "High Knees Running in Place"
         , rest
         , exc "Lunge"
         , rest
         , exc "Push-up With Rotation"
         , rest
         , exc "Side Plank"
         ]
        False
